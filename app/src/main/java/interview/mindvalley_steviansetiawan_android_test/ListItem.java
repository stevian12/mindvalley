package interview.mindvalley_steviansetiawan_android_test;

/**
 * Created by stevian on 7/11/16.
 */

public class ListItem {

    private String title;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "[ title=" + title + "]";
    }
}
