package interview.mindvalley_steviansetiawan_android_test;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

//import com.wangjie.androidbucket.utils.ABTextUtil;
//import com.wangjie.androidbucket.utils.imageprocess.ABShape;
//import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
//import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
//import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
//import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
//import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

public class MainActivity extends Activity { //implements RapidFloatingActionContentLabelList.OnRapidFloatingActionContentLabelListListener

    private SwipeRefreshLayout swipeContainer;

    //Sorry, Floating action button is not working because of library's dependencies issue :(
//    private RapidFloatingActionLayout rfaLayout;
//    private RapidFloatingActionButton rfaButton;
//    private RapidFloatingActionHelper rfabHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Pull to refresh
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchTimelineAsync();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //Floating action button init
//        rfaLayout = (com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout) findViewById(R.id.label_list_sample_rfal);
//        rfaButton = (com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton) findViewById(R.id.label_list_sample_rfab);
//        SetRFButton();

        ArrayList<ListItem> listData = getListData();

        final ListView listView = (ListView) findViewById(R.id.custom_list);
        listView.setAdapter(new CustomListAdapter(this, listData));
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                ListItem newsData = (ListItem) listView.getItemAtPosition(position);
                Toast.makeText(MainActivity.this, "Selected :" + " " + newsData, Toast.LENGTH_LONG).show();
            }
        });
    }

    private ArrayList<ListItem> getListData() {
        ArrayList<ListItem> listMockData = new ArrayList<ListItem>();
        String[] images = getResources().getStringArray(R.array.images_array);
        String[] headlines = getResources().getStringArray(R.array.title_array);

        for (int i = 0; i < images.length; i++) {
            ListItem newsData = new ListItem();
            newsData.setUrl(images[i]);
            newsData.setTitle(headlines[i]);
            listMockData.add(newsData);
        }
        return listMockData;
    }

    public void fetchTimelineAsync() {
        ShowLists();
        swipeContainer.setRefreshing(false);
    }

//    public void SetRFButton(){
//        RapidFloatingActionContentLabelList rfaContent = new RapidFloatingActionContentLabelList(getApplicationContext());
//        rfaContent.setOnRapidFloatingActionContentLabelListListener(this);
//        List<RFACLabelItem> items = new ArrayList<>();
//        items.add(new RFACLabelItem<Integer>()
//                .setLabel("Show Lists")
//                .setResId(R.mipmap.ic_refresh_white_24dp)
//                .setIconNormalColor(0xffd84315)
//                .setIconPressedColor(0xffbf360c)
//                .setWrapper(1)
//
//        );
//        rfaContent
//                .setItems(items)
//                .setIconShadowRadius(ABTextUtil.dip2px(getApplicationContext(), 5))
//                .setIconShadowColor(0xff888888)
//                .setIconShadowDy(ABTextUtil.dip2px(getApplicationContext(), 5))
//        ;
//
//        rfabHelper = new RapidFloatingActionHelper(
//                getApplicationContext(),
//                rfaLayout,
//                rfaButton,
//                rfaContent
//        ).build();
//    }
//
//    //Floating action button listener
//    @Override
//    public void onRFACItemLabelClick(int position, RFACLabelItem item) {
//        switch(position) {
//            case 0 :
//                ShowLists();
//                break;
//        }
//        rfabHelper.toggleContent();
//    }
//
//    @Override
//    public void onRFACItemIconClick(int position, RFACLabelItem item) {
//        switch(position) {
//            case 0 :
//                ShowLists();
//                break;
//        }
//        rfabHelper.toggleContent();
//    }

    public void ShowLists () {
        ArrayList<ListItem> listData = getListData();

        final ListView listView = (ListView) findViewById(R.id.custom_list);
        listView.setAdapter(new CustomListAdapter(this, listData));
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                ListItem titleData = (ListItem) listView.getItemAtPosition(position);
                Toast.makeText(MainActivity.this, "Selected :" + " " + titleData, Toast.LENGTH_LONG).show();
            }
        });
    }

}
